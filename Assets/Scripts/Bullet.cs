﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject FXDestroy;

    // Update is called once per frame
    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun
    /// touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Suelo"){
            Instantiate(FXDestroy,transform.position,transform.rotation,null);
            Destroy(gameObject,0.01f);
        }
    }
    
}
