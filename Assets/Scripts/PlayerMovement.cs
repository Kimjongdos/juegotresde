﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rbd;
    public float speed;
    public int Score = 0;
    public ScoreManager scoreManager;
    public Animator anim;
    private Vector3 Direccion;
    public bool Run = false;
    public Image image;
    public float Vida = 1;
    private float maxVida = 100f;
    public float VPS = 10f;
    public float tiempoConVida = 0;
    public TextMeshProUGUI LifeTime;


    
    void Start(){
        Vida = maxVida;
        image.fillAmount = maxVida;
        image.fillAmount = image.fillAmount * 100;;
        

    }
   
    void Update(){
        anim.SetBool("Run",Run);
       // Debug.Log(image.fillAmount);
        image.fillAmount -= Time.deltaTime / VPS;
        
        
        if(Input.GetAxisRaw("Horizontal") > 0.1f && Input.GetAxisRaw("Vertical") > 0.1f){ //ARRIBA A LA DERECHA(W,D)
            Direccion += Vector3.forward + Vector3.right;
            transform.rotation = Quaternion.Euler(0,35,0);
           
        }
        if(Input.GetAxisRaw("Horizontal") < -0.1f && Input.GetAxisRaw("Vertical") > 0.1f){ //ARRIBA A LA IZQUIERDA(W,A)
            Direccion += Vector3.forward + Vector3.left;
            transform.rotation = Quaternion.Euler(0,-35,0);
           
        }

        if(Input.GetAxisRaw("Horizontal") < -0.1f && Input.GetAxisRaw("Vertical") < 0.1f){ //ABAJO A LA IZQUIERDA(S,A)
            Direccion -= Vector3.forward + Vector3.left;
            transform.rotation = Quaternion.Euler(0,-135,0);
           
        }

        if(Input.GetAxisRaw("Horizontal") > 0.1f && Input.GetAxisRaw("Vertical") < -0.1f){ //ABAJO A LA IZQUIERDA(S,A)
            Direccion -= Vector3.forward + Vector3.left;
            transform.rotation = Quaternion.Euler(0,135,0);
           
        }

        Vida = image.fillAmount;
    }
    
    void FixedUpdate(){
        ScoreManager();
        Movement();
        LifeManager();
    }
     public void Movement(){
        bool H = Input.GetButton("Horizontal");
        bool V = Input.GetButton("Vertical");
        
        if(H || V){
            Run = true;
        }else{
            Run = false;
        }
        

        Vector3 Direccion = Vector3.zero;
        //speed += Time.deltaTime * 8;
        if(Input.GetKey(KeyCode.W)){
            Direccion += Vector3.forward;
            transform.rotation = Quaternion.Euler(0,0,0);
            
        }
        if(Input.GetKey(KeyCode.S)){
            Direccion -= Vector3.forward;
            transform.rotation = Quaternion.Euler(0,-180,0);
            
        }
        if(Input.GetKey(KeyCode.A)){
            Direccion += Vector3.left;    
            transform.rotation = Quaternion.Euler(0,-90,0);      
           
        }

        if(Input.GetKey(KeyCode.D)){
            Direccion += Vector3.right;
            transform.rotation = Quaternion.Euler(0,90,0);   
            
        }


        //Movimiento Diago9nañl
        


        
        rbd.velocity = Direccion * speed * Time.deltaTime;
        
    }
    public void LifeManager(){
       if(Vida<=0){
           gameObject.SetActive(false);
       }
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Bullet"){
           Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Punto"){
            Debug.Log("Colision");
            Score = Score + 5;
            image.fillAmount += 0.3f;
            scoreManager.ManagerScore(Score);
            Destroy(other.gameObject);
        }
    }

    void ScoreManager(){
        tiempoConVida += Time.deltaTime;
        LifeTime.text = tiempoConVida.ToString("0.0");
    }

   
    
}
